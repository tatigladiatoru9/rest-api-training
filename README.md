# NodeJS REST Api :

### Installing required programs

- [_nodeJS here_](https://nodejs.org/en/download/)
- [_Postman here_](https://www.getpostman.com/downloads/)
- [_XAMPP here_](https://www.apachefriends.org/ro/index.html)

### Steps

1.  Create a folder.
1.  Open the terminal in the folder.
1.  Install npm required npm packages

`npm install -s express` (required for the enviornment of the app)
`npm install -s body-parser` (required for retrieving body in JSON)
`npm install -s mysql2`(required for mysql installation)
`npm install -s sequelize`(required for using MySQL with JavaScript syntax)

Optional
`npm install -g nodemon`(automatically restarts the server when saving the code)

4. Create a file named server.js
5. Write this code in server.js

`const express = require('express')` Importing express framework in a const
`const bodyParser = require('body-parser')` Importing body-parser in a const
(body-parser is used for retrieving the requests in a JSON)
`const router = require('./routes')` (import routes after creating the routes folder, not now)
`const app = express();` Initializing an express instance for the app
`const port = 8080` Setting the port for the server
`app.use(bodyParser.json())` Transforming bodies of the requests in JSON object

```
app.get('/', (req, res) => {
    res.status(200).send('Serveru merge') Output sent on localhost:8080
})
```

`app.use('/api', router)` Servers files on /api/...(routes)

```
app.listen(port, () => {
    console.log('Serverul ruleaza pe portul: ' + port) Turn on server function
})
```

6. Create a folder named config, in this folder create a javascript file named db.js
   Follow the code in db:

```
const Sequelize = require('sequelize')
**Sequelize allows writing database code in JavaScript syntax**
```

```
const sequelize = new Sequelize("rest", "root", "", {
    dialect: "mysql",
    host: "localhost",
    define: {
        timestamps: true
    }   (Create a new Sequelize object)
})
module.exports = sequelize (Exports the sequelize object for later usage)
```

7. Create a folder named models. Here you will have to define the model for the user. The fields in the user object. Create user.js file and write down the code below:

   ```
   module.exports = (sequelize, DataTypes) => {
   return sequelize.define('user', {
       user_name: DataTypes.STRING,
       email: DataTypes.STRING,
       phone: DataTypes.STRING,
       password: DataTypes.STRING
   }, {
       underscored: true
   })
   }
   ```

8. Create a new file in the models folder named index.js.
   The "index" usually gathers all the files in a folder and exports them for later use. You will see this index very often.

```
//index.js
const db = require('../config/db') // Retrieves the database configuration from config folder
const User = db.import('./users') // Importing the user model for our database

module.exports = { //Export for later use
    User,
    connection: db
}
```

9. Create a new folder named routes. In this folder we will write the routes...Create other.js and write down the code below:

```
const express = require('express')
const router = express.Router()
const connection = require('../models').connection

router.get('/reset', async (req, res) => {
    connection.sync({
            force: true
        })
        .then(() => {
            res.status(201).send({
                message: 'S-a resetat baza de date'
            })
        })
        .catch(() => {
            res.status(500).send({
                message: 'Eroare la resetarea BD'
            })
        })
})

module.exports = router
```

10. Create a new file in the routes folder named index.js.

```
const express = require('express')
const router = express.Router()
const otherRouter = require('./other') //Route for resetting the database.

router.use('/', otherRouter)

module.exports = router
```

11. Now enter in terminal and start the server:
    `npm start`

12. Open postman. Follow the instructions from the image below.
    ![postman](https://i.ibb.co/CznXRFX/BoA-9t4t.png)

13. Now verify the database and you will see that the tabels were crated.
14. In routes folder create users.js and write this code:

```
const express = require("express")
const router = express.Router();
const UserDB = require('../models').User
```

Adding the register api in users.js:

```
router.post('/register', async (req, res) => {
    const errors = [] // We create an empty array where we will save the errors if any

    const user = { //We create an object with this keys(the same from the user model)
        user_name: req.body.user_name,
        email: req.body.email,
        phone: req.body.phone,
        password: req.body.password
    }

    if (!user.user_name) { // check if the name was introduced
        errors.push('Nu ai introdus numele utilizatorului')
    } else if (!user.user_name.match(/([A-ZAÎ??Â])+(?=.{1,40}$)[a-zA-ZAÎ??Âaî??]+(?:[-\\s][a-zA-ZAÎ??Âaî??â]+)*$/)) {
        errors.push('Invalid user name!')
    }

    if (!user.email) { // check if the email was introduced
        errors.push('Nu ai introdus email-ul!')
    } else if (!user.email.match(/[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/)) {
        errors.push('Format invalid email!')
    }

    if (!user.phone) { // check if the phone was introduced
        errors.push('Nu ai introdus numarul de telefon!')
    } else if (!user.phone.match(/^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/)) {
        errors.push('Formatul numarului de telefon este invalid!')
    }

    if (!user.password) { // check if the password was introduced
        errors.push('Nu ai introdus parola')
    } else if (!user.password.match(/^.{4,40}$/)) {
        errors.push('Parola trebuie sa contina intre 4 si 40 de caractere')
    }

    if (errors.length === 0) {// check if the errors array is empty
        try {
            await UserDB.create(user)
            res.status(201).send({
                message: 'Utilizator adaugat!'
            })
        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: 'Eroare la crearea unei utilizator!'
            })
        }
    } else {
        res.status(400).send({
            errors
        })
    }

})
```

Adding the show users api:

```
router.get('/users', async (req, res) => {
    UserDB.findAll({
            attributes: ['user_name', 'email'] // show the user_name and email for every user
        }).then(users => {
            res.status(200).send(users)
        })
        .catch(() => {
            res.status(500).send({
                message: "Eroare baza de date"
            })
        })
})
```

Adding the update user api:

```
router.put('/update/:id', async (req, res) => {
    try {
        const user = await UserDB.findOne({
            where: {
                id: req.params.id
            }
        })
        if (user) {
            if (!user.password) {
                res.status(400).send({
                    message: "Nu ai introdus parola noua"
                })
            } else if (!user.password.match(/^.{4,40}$/)) {
                res.status(400).send({
                    message: "Parola trebuie sa aiba intre 4 si 40 de caractere"
                })
            } else {
                user.update({
                        password: req.body.password
                    })
                    .then(() => {
                        res.status(200).send({
                            message: "Ok, utilizator updatat"
                        })
                    })
            }
        }
    } catch {
        res.status(500).send({
            message: 'Server crapat'
        })
    }
})

module.exports = router;
```

15. Open index.js from routes and add:
    `const usersRouter = require('./users')`

`router.use('/', usersRouter)`

16. In postman, for a POST request follow the image below:
    ![postman](https://i.ibb.co/TgP8vH3/DxHuVdBR.jpg)

17. Retrieve all the information in the database with a GET request in Postman.
    Use the path:`localhost:8080/api/users`then send the request
18. For updating information on a specific user use a PUT request in Postman.
    Use the path: `localhost:8080/api/update/:id`
    Let's say you want to update the 2nd user. Your request will look like this:
    `localhost:8080/api/update/2`
    You get the idea.

# Database schema : https://dbdesigner.page.link/ahLTYJg1F1KaBagJ7

_Note: Enter xampp control panel, turn on Apache and MySQL_
_Access in browser:_ localhost/phpmyadmin
_Create a database named "rest"_

# User requests:

_Note: Use Postman for sending requests_

- _GET /api/reset_
  - **resets database and creates tables automatically**
- _POST /api/register_
  - **creates a new user in the database**
  - **required POST JSON:**
    {
    "userName" : "text",
    "email" : "text",
    "phone" : "text",
    "password" : "text"
    }
- GET /api/users
  - **returns all the users in the database**
- PUT /api/update/:id
  - **Updates specified user information**
