const express = require('express')
const router = express.Router()
const connection = require('../models').connection

router.get('/reset', async (req, res) => {
    connection.sync({
            force: true
        })
        .then(() => {
            res.status(201).send({
                message: 'S-a resetat baza de date'
            })
        })
        .catch(() => {
            res.status(500).send({
                message: 'Eroare la resetarea BD'
            })
        })
})

module.exports = router